var validator = require('validator');

var validateHelper = (function () {
    return {
    
        isMobilePhone : function(mobile){
            return validator.isMobilePhone(mobile, "en-IN");
        },

    };
})();

module.exports = validateHelper;
