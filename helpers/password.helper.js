var bcrypt = require('bcrypt');

var passwordConstant = require('../constants/password.constant');

var passwordHelper = (function () {
    return {
        encryptPassword: function (password) {
            return bcrypt.hash(password, passwordConstant.SALT_ROUND)
        },

        matchPassword: function (inputPassword, storedPassword) {
            return bcrypt.compare(inputPassword, storedPassword)
        },

    };
})();

module.exports = passwordHelper;
