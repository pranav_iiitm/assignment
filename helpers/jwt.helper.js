var Promise = require('bluebird');
var jwt = require('jwt-simple');
var moment = require('moment');

var jwtConstant = require('../constants/jwt.constant');

var AuthorisationFailedError = require('../customErrors/AuthorisationFailedError');
var errorCodeList = (require('../constants/error.constant').ERROR_CODE_LIST);

var jwtHelper = (function () {
    return {
        decodeToken: function (token) {
            return new Promise(function (resolve, reject) {
                var data = jwt.decode(token, jwtConstant.JWT_TOKEN_SECRET);
                resolve(data);
            })
                .catch(function (error) {
                    console.log(error);
                    throw new AuthorisationFailedError(errorCodeList.AuthorisationFailedError);
                })
        },

        createToken: function (data) {
            console.log(data);
            return new Promise(function (resolve, reject) {
                var expires = moment().add(jwtConstant.TIME_DENOMINATION, jwtConstant.EXPIRY_VALUE).valueOf();
                data.expires = expires;
                newJwtToken = jwt.encode(data, jwtConstant.JWT_TOKEN_SECRET);
                resolve(newJwtToken);
            })
        }
    };

})();

module.exports = jwtHelper;