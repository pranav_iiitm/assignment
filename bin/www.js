var app = require('../app');
var http = require('http');
var https = require('https');
var Promise = require('bluebird');
var mongoose = require('mongoose'); // ORM Module to bridge MongoDB and node.js




/*
 +++++++++++++++++++++++++++++++
 +    DATABASE CONNECTION      +
 +++++++++++++++++++++++++++++++
 */
var options = {
    promiseLibrary: Promise,
    useMongoClient: true,
}

var MONGO_URL = 'mongodb://localhost/'+process.env.DB_NAME;

console.log(MONGO_URL);
mongoose.connect(MONGO_URL, options, function (err) {
    if (err) throw err;
});
console.log('Mongoose connection status code:', mongoose.connection.readyState);


/**
 * +++++++++++++++++++++++++++++
 * +    START HTTP SERVER      +
 * +++++++++++++++++++++++++++++
 *
 */
app.set('port', process.env.PORT || 8000);
http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});

/**
 * ++++++++++++++++++++++++++++++
 * +    START HTTPS SERVER      +
 * ++++++++++++++++++++++++++++++
 *
 */
//TODO



/*
 ++++++++++++++++++++++++++++++++++++++++
 +     Application Level Middleware     +
 ++++++++++++++++++++++++++++++++++++++++
 */

app.use(function (err, req, res, next) {
    console.log(err.stack);
    res.status(500).send(err.stack);
});


