
var Instrument = require('../models/instrument.model');

var instrumentDao = (function() {
  return {
       
        getInstrument : function(findParam, selectParam, options){
            if(options.multi){
                return Instrument.find(findParam,selectParam).exec();
            }
            else{
                return Instrument.findOne(findParam,selectParam).exec();
            }
            
        },

        getInstrumentInstrumentvanceQuery : function(findParam, selectParam, options, sortParam, skipParam, limitParam){
            if(options.multi){
                return Instrument.find(findParam,selectParam).sort(sortParam).skip(skipParam).limit(limitParam).exec();
            }
            else{
                return Instrument.findOne(findParam,selectParam).sort(sortParam).skip(skipParam).limit(limitParam).exec();
            }
        },

        
        createInstrument : function(newInstrument){
            return (new Instrument(newInstrument).save());
        },

       
        updateInstrument : function(findParameter, updateParameter, InstrumentditionalOptions){
            return Instrument.findOneAndUpdate(findParameter, updateParameter, InstrumentditionalOptions).exec();
        },


        
         deleteInstrument : function(condition){
            return Instrument.find(condition).remove().exec();
        },


    };

})();

module.exports = instrumentDao;