
var User = require('../models/user.model');

var userDao = (function() {
  return {
       
        getUser : function(findParam, selectParam, options){
            if(options.multi){
                return User.find(findParam,selectParam).exec();
            }
            else{
                return User.findOne(findParam,selectParam).exec();
            }
            
        },

        getUserUservanceQuery : function(findParam, selectParam, options, sortParam, skipParam, limitParam){
            if(options.multi){
                return User.find(findParam,selectParam).sort(sortParam).skip(skipParam).limit(limitParam).exec();
            }
            else{
                return User.findOne(findParam,selectParam).sort(sortParam).skip(skipParam).limit(limitParam).exec();
            }
        },

        
        createUser : function(newUser){
            return (new User(newUser).save());
        },

       
        updateUser : function(findParameter, updateParameter, UserditionalOptions){
            return User.findOneAndUpdate(findParameter, updateParameter, UserditionalOptions).exec();
        },


        
         deleteUser : function(condition){
            return User.find(condition).remove().exec();
        },


    };

})();

module.exports = userDao;