var mongoose = require('mongoose');
var instrumentSchema = new mongoose.Schema({
    name : String,
    quantity: Number,
    flag : {type: Number, enum : [0,1,2], default:1}, // 0 => Inactive, 1=> Active, 2=> Deleted
},
    {
        timestamps: true
    });
var Instrument = mongoose.model("Instrument", instrumentSchema, "instruments");

module.exports = Instrument;


