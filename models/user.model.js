var mongoose = require('mongoose');
var userSchema = new mongoose.Schema({
    name: String,
    mobile: String,
    password: String,
    flag : {type: Number, enum : [0,1,2], default:1}, // 0 => Inactive, 1=> Active, 2=> Deleted
},
    {
        timestamps: true
    });
var User = mongoose.model("User", userSchema, "users");

module.exports = User;


