var jwtConstant = (function() {
  return {
      JWT_TOKEN_SECRET : "assignment",
      EXPIRY_VALUE : 1 ,
      TIME_DENOMINATION : "days"
  };
})();

module.exports = jwtConstant;
