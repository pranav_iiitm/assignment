var passwordConstant = (function () {
    return {
        SALT_ROUND : 10
    };
})();

module.exports = passwordConstant;
