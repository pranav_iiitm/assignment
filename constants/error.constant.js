var errorConstant = (function () {
    return {
        ERROR_CODE_LIST: {
            "UserAlreadyExistsError": 9001,
            "LoginFailedError" : 9002,
            "AuthorisationFailedError": 401,
            "InstrumentNotExistsError" : 9003
        }
    };
})();

module.exports = errorConstant;
