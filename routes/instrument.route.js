var express = require('express');
var router = express.Router();

var instrumentController = require('../controllers/instrument.controller');
var InvalidValueError = require("../customErrors/InvalidValueError");
var errorCodeList = (require('../constants/error.constant').ERROR_CODE_LIST);
var errorHelper = require('../helpers/error.helper');
var validateHelper = require('../helpers/validate.helper.js')


var instrumentRouteController = (function () {
    return {
       createInstrument: function (req, res) {
            try {
                let rawData = req.body.rawData;

                if (!rawData) throw new InvalidValueError("<rawData>", "<null>", errorCodeList.InvalidValueError);
                
                return instrumentController.createInstrument(rawData)
                    .then(function (result) {
                        res.status(200).json({ status: "Success", data: result });
                    })
                    .then(undefined, function (err) {
                        if (err) console.log(err);
                        res.status(400).json(errorHelper.formatError(err));
                    })
            } catch (err) {
                if (err) console.log(err);
                res.status(400).json(errorHelper.formatError(err));
            }
        },

        getInstrument : function (req, res) {
            try {

                return instrumentController.getInstrument()
                    .then(function (result) {
                        res.status(200).json({ status: "Success", data: result });
                    })
                    .then(undefined, function (err) {
                        if (err) console.log(err);
                        res.status(400).json(errorHelper.formatError(err));
                    })
            } catch (err) {
                if (err) console.log(err);
                res.status(400).json(errorHelper.formatError(err));
            }
        },


        getInstrumentById : function (req, res) {
            try {
                let id = req.params.id;

                return instrumentController.getInstrumentById(id)
                    .then(function (result) {
                        res.status(200).json({ status: "Success", data: result });
                    })
                    .then(undefined, function (err) {
                        if (err) console.log(err);
                        res.status(400).json(errorHelper.formatError(err));
                    })
            } catch (err) {
                if (err) console.log(err);
                res.status(400).json(errorHelper.formatError(err));
            }
        },
        
    };
})();

module.exports = instrumentRouteController;





