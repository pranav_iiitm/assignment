
/*
 ++++++++++++++++++++++++++++++++++++++++
 +     LIST OF ROUTE'S CONTROLLERS      +
 ++++++++++++++++++++++++++++++++++++++++
 */

var loginRouteController = require('./login.route');
var instrumentRouteController = require('./instrument.route');


/*
 ++++++++++++++++++++++++++++++++
 +     LIST OF CONTROLLERS      +
 ++++++++++++++++++++++++++++++++
 */
var authController = require('../controllers/auth.controller');


module.exports = function (app) {

 /*
        +++++++++++++++++
        +     LOGIN     +
        +++++++++++++++++
     */

    app.post('/api/v1/signUp', loginRouteController.signUp);
    app.post('/api/v1/login', loginRouteController.login);

/*
        ++++++++++++++++++++++
        +     INSTRUMENT     +
        ++++++++++++++++++++++
     */

    app.get('/api/v1/instrument', authController.validateToken, instrumentRouteController.getInstrument);
    app.get('/api/v1/instrument/:id', authController.validateToken, instrumentRouteController.getInstrumentById);
    app.post('/api/v1/instrument', authController.validateToken, instrumentRouteController.createInstrument);




}


