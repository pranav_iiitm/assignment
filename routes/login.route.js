var express = require('express');
var router = express.Router();

var loginController = require('../controllers/login.controller');
var InvalidValueError = require("../customErrors/InvalidValueError");
var errorCodeList = (require('../constants/error.constant').ERROR_CODE_LIST);
var errorHelper = require('../helpers/error.helper');
var validateHelper = require('../helpers/validate.helper.js')


var loginRouteController = (function () {
    return {
       signUp: function (req, res) {
            try {
                let name = req.body.name;
                let mobile = req.body.mobile;
                let password = req.body.password;

                if (!name) throw new InvalidValueError("<name>", "<null>", errorCodeList.InvalidValueError);
                if (!mobile) throw new InvalidValueError("<mobile>", "<null>", errorCodeList.InvalidValueError);
                if (!password) throw new InvalidValueError("<password>", "<null>", errorCodeList.InvalidValueError);

                
                if (!validateHelper.isMobilePhone(mobile)) throw new InvalidValueError("<userName>", mobile, errorCodeList.InvalidValueError);

                return loginController.signUp(name, mobile, password)
                    .then(function (result) {
                        res.status(200).json({ status: "Success", data: result });
                    })
                    .then(undefined, function (err) {
                        if (err) console.log(err);
                        res.status(400).json(errorHelper.formatError(err));
                    })
            } catch (err) {
                if (err) console.log(err);
                res.status(400).json(errorHelper.formatError(err));
            }
        },


        login : function (req, res) {
            try {

                let userName = req.body.userName;
                let password = req.body.password;

                if (!userName) throw new InvalidValueError("<userName>", "<null>", errorCodeList.InvalidValueError);
                if (!password) throw new InvalidValueError("<password>", "<null>", errorCodeList.InvalidValueError);

                if (!validateHelper.isMobilePhone(userName)) throw new InvalidValueError("<userName>", userName, errorCodeList.InvalidValueError);

                return loginController.login(userName, password)
                    .then(function (result) {
                        res.setHeader('token', result);
                        res.status(200).json({ status: "Success", data: {} });
                    })
                    .then(undefined, function (err) {
                        if (err) console.log(err);
                        res.status(400).json(errorHelper.formatError(err));
                    })
            } catch (err) {
                if (err) console.log(err);
                res.status(400).json(errorHelper.formatError(err));
            }
        },


        
    };
})();

module.exports = loginRouteController;





