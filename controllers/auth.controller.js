var Promise = require('bluebird');
var moment = require('moment');
var _ = require('underscore')

var userDao = require('../dao/user.dao');
var jwtHelper = require('../helpers/jwt.helper');
var errorHelper = require('../helpers/error.helper');
var AuthorisationFailedError = require('../customErrors/AuthorisationFailedError');
var errorCodeList = (require('../constants/error.constant').ERROR_CODE_LIST);

var authController = (function () {
    return {
        validateToken: function (req, res, next) {
            var token = req.headers.token || req.query.t;
            
            var userData = null;

            return jwtHelper.decodeToken(token)
                .then(function (decode) {
                    if (moment().isAfter(decode.expires)) throw new AuthorisationFailedError(errorCodeList.AuthorisationFailedError);
                    // Person Id is a Must.
                    if (!(decode.personId)) throw new AuthorisationFailedError(errorCodeList.AuthorisationFailedError);

                    // temporary store user Data in userData variable
                    userData = decode;

                    var pValidator = [];
                    if (decode.hasOwnProperty('personId')) pValidator.push(userDao.getUser({ '_id': decode.personId, 'flag': 1 }, {}, { multi: false }));
                    return Promise.all(pValidator)
                })
                .then(function (result) {
                    for (var i = 0; i < result.length; i++) {
                        if (!result[i]) throw new AuthorisationFailedError(errorCodeList.AuthorisationFailedError);
                    }
                    // add userData in req.body under user property.
                    req.body.user = userData;
                    next();
                })
                .then(undefined, function (err) {
                    console.log(err);
                    res.status(400).json(errorHelper.formatError(err));
                })

        }


    };

})();

module.exports = authController;