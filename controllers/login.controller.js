/*      
   +++++++++++++++++++++++
   +     NPM MODULES     +
   +++++++++++++++++++++++
*/
var Promise = require('bluebird');

/*      
   +++++++++++++++++++++++
   +     CONTROLLERS     +
   +++++++++++++++++++++++
*/


/*      
   +++++++++++++++
   +     DAO     +
   +++++++++++++++
*/
var userDao = require('../dao/user.dao');


/*      
   +++++++++++++++++++
   +     CONSTANT    +
   +++++++++++++++++++
*/
var errorCodeList = (require('../constants/error.constant').ERROR_CODE_LIST);


/*      
   +++++++++++++++++++
   +     HELPERS     +
   +++++++++++++++++++
*/
var passwordHelper = require('../helpers/password.helper');
var jwtHelper = require('../helpers/jwt.helper');

/*      
   ++++++++++++++++++
   +     ERRORS     +
   ++++++++++++++++++
*/
var CustomErrors = require("../customErrors/CustomError");



var loginController = (function () {
    return {

        signUp: function (name, mobile, password) {

            return loginController.ifUserAlreadyExists(mobile)
                .then(function (ifUserAlreadyExists) {
                    if (ifUserAlreadyExists) throw new CustomErrors("User with mobile <" + mobile + "> already exists! ", errorCodeList.UserAlreadyExistsError);
                    // encrypt the password
                    return passwordHelper.encryptPassword(password)
                })
                .then(function (hashedPassword) {
                    var newUser = {
                        name: name,
                        mobile: mobile,
                        password: hashedPassword
                    }
                    return userDao.createUser(newUser)
                })
                .then(function (newUser) {
                    return {
                        meassge: "User Created!"
                    }
                })


        },


        login: function (mobile, password) {

            var personId;
            var findParam = { 'mobile': mobile };
            var options = { 'multi': false };

            return userDao.getUser(findParam, null, options)
                .then(function (user) {
                    if (!user) throw new CustomErrors("Authentication Failed", errorCodeList.LoginFailedError);
                    personId = user._id;
                    return passwordHelper.matchPassword(password, user.password)
                })
                .then(function (isPasswordMatched) {
                    if (!isPasswordMatched) throw new CustomErrors("Authentication Failed", errorCodeList.LoginFailedError);
                    return jwtHelper.createToken({ 'personId': personId })
                })
                .then(function (jwtToken) {
                    if (!jwtToken) throw new InternalServerError(errorCodeList.InternalServerError);
                    return jwtToken;
                })
        },


        ifUserAlreadyExists: function (mobile) {
            return userDao.getUser({ 'mobile': mobile }, { '_id': 1 }, { multi: false })
                .then(function (user) {
                    if (user) return true;
                    return false;
                })
        }

    };

})();

module.exports = loginController;