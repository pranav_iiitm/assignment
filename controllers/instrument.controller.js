/*      
   +++++++++++++++++++++++
   +     NPM MODULES     +
   +++++++++++++++++++++++
*/
var Promise = require('bluebird');

/*      
   +++++++++++++++++++++++
   +     CONTROLLERS     +
   +++++++++++++++++++++++
*/


/*      
   +++++++++++++++
   +     DAO     +
   +++++++++++++++
*/
var instrumentDao = require('../dao/instrument.dao');


/*      
   +++++++++++++++++++
   +     CONSTANT    +
   +++++++++++++++++++
*/
var errorCodeList = (require('../constants/error.constant').ERROR_CODE_LIST);


/*      
   +++++++++++++++++++
   +     HELPERS     +
   +++++++++++++++++++
*/


/*      
   ++++++++++++++++++
   +     ERRORS     +
   ++++++++++++++++++
*/
var CustomErrors = require("../customErrors/CustomError");



var instrumentController = (function () {
    return {

        createInstrument : function(rawData){
            var newInstrumentData = instrumentController.extractInstrumentDataFromRawdata(rawData);
            
            return instrumentDao.getInstrument({'name' : newInstrumentData.instrumentName}, null, {multi : false})
            .then(function(instrument){
                if(!instrument) {
                    var newInstrument = {};
                    if(newInstrumentData.boughtOrSold == 2)  newInstrument.quantity = 0 - newInstrumentData.size;
                    else newInstrument.quantity = newInstrumentData.size;
                    newInstrument.name = newInstrumentData.instrumentName;
                    return instrumentDao.createInstrument(newInstrument);
                }
                else{
                    var updateParam = {};
                    if(newInstrumentData.boughtOrSold == 2) {
                        updateParam = {$inc:{'quantity': -newInstrumentData.size}}
                    }
                    else{
                        updateParam = {$inc:{'quantity': newInstrumentData.size}}
                    }
                    console.log(updateParam);
                    return instrumentDao.updateInstrument({'name' : instrument.name}, updateParam, {new : true} )
                    .then(function(updatedInstrument){
                        return updatedInstrument;
                    })
                }
            })
        },


        getInstrument : function(){
            return instrumentDao.getInstrument({}, {'name' : 1, 'quantity' : 1}, {multi : true})
            .then(function(data){
                return data;
            })
        },


        getInstrumentById : function(id){
            return instrumentDao.getInstrument({'name' : id}, {'name' : 1, 'quantity' : 1}, {multi : false})
            .then(function(data){
                if(!data) throw new CustomErrors("Instuument with id <" + id + "> doesn't exists!", errorCodeList.InstrumentNotExistsError);
                return data;
            })
        },

        extractInstrumentDataFromRawdata : function(rawData){
            var map = new Map();
            var data = rawData;
            var split_data = data.split(":")[1].trim().split("|");
            for(var i=0;i<split_data.length;i++){
                var split_req = split_data[i].split("=");
                map.set(split_req[0], split_req[1]);
            }
            // console.log(map);

            var newInstrumentData = {
                size : map.get('32'),
                boughtOrSold : map.get('54'),
                instrumentName : map.get('48')
            }

            return newInstrumentData;
        },



    };

})();

module.exports = instrumentController;