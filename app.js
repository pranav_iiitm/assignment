var express = require('express');
var path = require('path');
var logger = require('morgan'); // Logging middleware module to log requests and responses on console.
var body_parser = require('body-parser'); // Module to parse JSON in requests to all APIs.
var _ = require('underscore');
var cors = require('cors'); // Cross origin resource sharing module to allow non-same origin apps to make request with server.
var moment = require('moment'); // Module to generate and parse Date objects.
var helmet = require('helmet');   //Helmet helps secure Express apps by setting various HTTP headers.
var mongoose = require('mongoose');
var Promise = require('bluebird');
var ejs = require('ejs');

var app = express();

app.set('views', path.join(__dirname, 'templates'));

app.set('view engine', 'ejs');


app.use(cors());
app.use(helmet());
app.use(logger('dev'));
app.use(body_parser.json());
app.use(body_parser.raw());
app.use(body_parser.urlencoded({ extended: false }));

app.all('/*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "X-Requested-With , token , x-super-admin");
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header("Access-Control-Expose-Headers", "token , x-super-admin");
    next();
});


var routes = require('./routes')(app);

module.exports = app;